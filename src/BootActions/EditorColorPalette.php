<?php

namespace EliKellendonk\WpUtil\BootActions;

use EliKellendonk\WpUtil\Proxy\WordpressProxy;
use EliKellendonk\WpUtil\Runnable;

class EditorColorPalette implements Runnable
{
    private WordpressProxy $wp;
    private int $wpActionPriority;
    private array $colorPalette;

    public function __construct(
        WordpressProxy $wp,
        int $wpActionPriority,
        array $colorPalette
    ) {
        $this->wp = $wp;
        $this->wpActionPriority = $wpActionPriority;
        $this->colorPalette = $colorPalette;
    }

    public function run(): void
    {
        $this->wp->addAction(
            'after_setup_theme',
            fn() => $this->wp->addThemeSupport(
                'editor-color-palette',
                $this->colorPalette,
            ),
            $this->wpActionPriority,
        );
    }
}
