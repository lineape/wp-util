<?php

namespace EliKellendonk\WpUtil\BootActions;

use EliKellendonk\WpUtil\Runnable;
use EliKellendonk\WpUtil\Proxy\WordpressProxy;
use WP_Rest_Cache_Plugin\Includes\Caching\Caching;

class WpApiMenuCaching implements Runnable
{
    private WordpressProxy $wp;
    private int $wpMenuActionPriority;
    private int $wpRestCacheEndpointsFilterPriority;
    private bool $autoClearCaches;

    public function __construct(
        WordpressProxy $wp,
        int $wpMenuActionPriority,
        int $wpRestCacheEndpointsFilterPriority,
        bool $autoClearCaches = true
    ) {
        $this->wp = $wp;
        $this->wpMenuActionPriority = $wpMenuActionPriority;
        $this->wpRestCacheEndpointsFilterPriority = $wpRestCacheEndpointsFilterPriority;
        $this->autoClearCaches = $autoClearCaches;
    }

    public function run(): void
    {
        // register menus as an allowed endpoint for caching
        $this->wp->addFilter(
            'wp_rest_cache/allowed_endpoints',
            /**
             * @param array<string, string[]> $endpoints
             * @return array
             */
            fn(array $endpoints) => $this->addMenuToAllowedEndpoints(
                $endpoints,
            ),
            $this->wpRestCacheEndpointsFilterPriority,
        );

        if ($this->autoClearCaches) {
            // manually clear caches whenever a menu is created, updated, deleted, etc.
            $this->wp->addAction(
                'wp_create_nav_menu',
                fn() => $this->clearCache(),
                $this->wpMenuActionPriority,
            );
            $this->wp->addAction(
                'wp_update_nav_menu',
                fn() => $this->clearCache(),
                $this->wpMenuActionPriority,
            );
            $this->wp->addAction(
                'wp_delete_nav_menu',
                fn() => $this->clearCache(),
                $this->wpMenuActionPriority,
            );
            $this->wp->addAction(
                'wp_add_nav_menu_item',
                fn() => $this->clearCache(),
                $this->wpMenuActionPriority,
            );
            $this->wp->addAction(
                'wp_update_nav_menu_item',
                fn() => $this->clearCache(),
                $this->wpMenuActionPriority,
            );
        }
    }

    public function clearCache(): void
    {
        /**
         * @psalm-suppress UndefinedClass
         * @var object|null
         */
        $cache = Caching::get_instance();

        if ($cache) {
            /** @psalm-suppress MixedMethodCall */
            $cache->clear_caches();
        }
    }

    /**
     * @param array<string, string[]> $endpoints
     * @return array
     */
    public function addMenuToAllowedEndpoints(array $endpoints): array
    {
        if (!isset($endpoints['menus/v1'])) {
            $endpoints['menus/v1'] = [];
        }

        if (!in_array('menus', $endpoints['menus/v1'], true)) {
            $endpoints['menus/v1'][] = 'menus';
        }

        return $endpoints;
    }
}
