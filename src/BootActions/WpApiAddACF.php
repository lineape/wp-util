<?php

namespace EliKellendonk\WpUtil\BootActions;

use EliKellendonk\WpUtil\Runnable;
use EliKellendonk\WpUtil\Proxy\ACFProxy;
use EliKellendonk\WpUtil\Proxy\WordpressProxy;

class WpApiAddACF implements Runnable
{
    private WordpressProxy $wp;
    private ACFProxy $acf;
    /** @var string[] */
    private array $registerOnObjectTypes;

    /**
     * @param WordpressProxy $wp
     * @param ACFProxy $acf
     * @param string[] $registerOnObjectTypes
     */
    public function __construct(
        WordpressProxy $wp,
        ACFProxy $acf,
        array $registerOnObjectTypes
    ) {
        $this->wp = $wp;
        $this->acf = $acf;
        $this->registerOnObjectTypes = $registerOnObjectTypes;
    }

    public function run(): void
    {
        $this->wp->addAction('rest_api_init', fn() => $this->addToApi());
    }

    public function addToApi(): void
    {
        $this->wp->registerRestField($this->registerOnObjectTypes, 'acf_data', [
            'get_callback' =>
                /** @param array{id: int} $post */
                fn($post) => $this->acf->getFields($post['id']) ?: null,
        ]);
    }
}
