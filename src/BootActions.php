<?php

namespace EliKellendonk\WpUtil;

use DI\Definition\Reference;

/**
 * Class BootActions runs any registered actions when the plugin first starts. Child Runnables are run based on the
 * ordering priority given when the register function is used. Child Runnables register with this global
 * singleton in their container definition file @see BootActions::register()
 * @package EliKellendonk\WpUtil
 */
class BootActions implements Runnable
{
    private static array $actionReferences = [];

    /**
     * Registers a runnable with a given priority. Lowest priority runs first.
     *
     * @param Reference $action
     * @param int|null $priority
     */
    public static function register(
        Reference $action,
        ?int $priority = 10
    ): void {
        static::$actionReferences[] = [$action, $priority];
    }

    /**
     * Creates container definitions from the registered boot actions and returns them sorted by priority.
     * @return Reference[]
     */
    public static function createDefinitions(): array
    {
        /** @var Reference[] */
        return collect(static::$actionReferences)
            ->sort(fn(array $a, array $b) => (int) $a[1] - (int) $b[1])
            ->map(
                /** @param Reference[] $action */
                fn(array $action): Reference => $action[0],
            )
            ->toArray();
    }

    /** @var Runnable[] */
    private array $bootActions;

    /**
     * BootActions constructor.
     * @param Runnable[] $bootActions
     */
    public function __construct(array $bootActions)
    {
        $this->bootActions = $bootActions;
    }

    /**
     * Runs the boot actions sequentially.
     */
    public function run(): void
    {
        foreach ($this->bootActions as $action) {
            $action->run();
        }
    }
}
