<?php

namespace EliKellendonk\WpUtil\Proxy;

class WordpressProxyImpl implements WordpressProxy
{
    /** @inheritDoc */
    public function getOption(string $option, $default)
    {
        /** @psalm-suppress UndefinedFunction */
        return get_option($option, $default);
    }

    /** @inheritDoc */
    public function getPostMeta(
        ?int $postId,
        ?string $key = '',
        ?bool $single = false
    ) {
        /** @psalm-suppress UndefinedFunction */
        return get_post_meta($postId, $key, $single);
    }

    /** @inheritDoc */
    public function updateOption(string $option, $value, $autoload = null): bool
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var bool
         */
        return update_option($option, $value, $autoload);
    }

    /** @inheritDoc */
    public function addAction(
        string $name,
        callable $function,
        ?int $priority = 10,
        ?int $acceptedArgs = 1
    ): bool {
        /**
         * @psalm-suppress UndefinedFunction
         * @var bool
         */
        return add_action($name, $function, $priority, $acceptedArgs);
    }

    /** @inheritDoc */
    public function addFilter(
        string $name,
        callable $function,
        ?int $priority = 10,
        ?int $acceptedArgs = 1
    ): bool {
        /**
         * @psalm-suppress UndefinedFunction
         * @var bool
         */
        return add_filter($name, $function, $priority, $acceptedArgs);
    }

    /** @inheritDoc */
    public function getNavMenuItems($menu, ?array $args = [])
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var array|false
         */
        return wp_get_nav_menu_items($menu, $args);
    }

    /** @inheritDoc */
    public function getPost($post, $output = 'OBJECT', $filter = 'raw')
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var array|null
         */
        return get_post($post, $output, $filter);
    }

    /** @inheritDoc */
    public function getTerm(
        $term,
        $taxonomy = '',
        $output = 'OBJECT',
        $filter = 'raw'
    ) {
        /** @psalm-suppress UndefinedFunction */
        return get_term($term, $taxonomy, $output, $filter);
    }

    /** @inheritDoc */
    public function getSiteUrl(): string
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var string
         */
        return get_site_url();
    }

    /** @inheritDoc */
    public function getPermalink($post, bool $leaveName = false)
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var string|false
         */
        return get_permalink($post, $leaveName);
    }

    /** @inheritDoc */
    public function getPostType($arg): ?object
    {
        // it's a string, call the wordpress function
        if (is_string($arg)) {
            /**
             * @psalm-suppress UndefinedFunction
             * @var object|null
             */
            return get_post_type_object($arg);
        }

        // it's a post id
        if (is_int($arg)) {
            return $this->getPostType($this->getPost($arg));
        }

        // it's a \Wp_Post
        /**
         * @psalm-suppress UndefinedClass
         * @psalm-suppress DocblockTypeContradiction
         */
        if ($arg instanceof \WP_Post) {
            /** @psalm-suppress MixedArgument */
            return $this->getPostType($arg->post_type);
        }

        // it's an array with a post_type key and a string value
        if (
            is_array($arg) &&
            array_key_exists('post_type', $arg) &&
            is_string($arg['post_type'])
        ) {
            return $this->getPostType($arg['post_type']);
        }

        // nothing matches, return null
        return null;
    }

    /** @inheritDoc */
    public function addThemeSupport(string $feature, ...$args)
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var false|void
         */
        return add_theme_support($feature, ...$args);
    }

    /** @inheritDoc */
    public function getAuthorMeta(int $userId, string $field)
    {
        /** @psalm-suppress UndefinedFunction */
        return get_the_author_meta($field, $userId);
    }

    /** @inheritDoc */
    public function getAuthorPostCount(
        int $userId,
        string $postType = 'post',
        bool $publicOnly = true
    ): int {
        /** @psalm-suppress UndefinedFunction */
        return (int) count_user_posts($userId, $postType, $publicOnly);
    }

    /** @inheritDoc */
    public function registerPostType(string $postType, array $args = []): void
    {
        /** @psalm-suppress UndefinedFunction */
        register_post_type($postType, $args);
    }

    /** @inheritDoc */
    public function registerRestField(
        $object_type,
        $attribute,
        $args = []
    ): void {
        /** @psalm-suppress UndefinedFunction */
        register_rest_field($object_type, $attribute, $args);
    }

    /** @inheritDoc */
    public function getMediaImage(int $id, $size = 'thumbnail', $icon = false)
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var array|false
         */
        return wp_get_attachment_image_src($id, $size, $icon);
    }

    /** @inheritDoc */
    public function isAdmin(): bool
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var bool
         */
        return is_admin();
    }

    /** @inheritDoc */
    public function getCurrentScreen()
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var \WP_Screen|null
         */
        return get_current_screen();
    }

    /** @inheritDoc */
    public function escAttr(string $text): string
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var string
         */
        return esc_attr($text);
    }

    /** @inheritDoc */
    public function getAdjacentPost(
        $inSameTerm = false,
        $excludedTerms = '',
        $previous = true,
        $taxonomy = 'category'
    ) {
        /**
         * @psalm-suppress UndefinedDocblockClass
         * @psalm-suppress UndefinedFunction
         * @var null|string|\WP_Post
         */
        return get_adjacent_post(
            $inSameTerm,
            $excludedTerms,
            $previous,
            $taxonomy,
        );
    }
}
