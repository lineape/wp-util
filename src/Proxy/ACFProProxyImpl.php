<?php

namespace EliKellendonk\WpUtil\Proxy;

class ACFProProxyImpl extends ACFProxyImpl implements ACFProProxy
{
    /** @inheritDoc */
    public function registerBlock(array $block)
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var array|false
         */
        return acf_register_block_type($block);
    }
}
