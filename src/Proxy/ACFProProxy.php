<?php

namespace EliKellendonk\WpUtil\Proxy;

/**
 * ACFProProxy abstracts away the Wordpress ACF Pro API so that tests can be written.
 *
 * Any functions that are only available from ACF Pro go here
 */
interface ACFProProxy extends ACFProxy
{
    /**
     * @see acf_register_block_type()
     *
     * @param array $block
     * @return array|false
     */
    public function registerBlock(array $block);
}
