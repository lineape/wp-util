<?php

namespace EliKellendonk\WpUtil\Proxy;

interface YoastProxy
{
    /**
     * @see WPSEO_Meta::get_value()
     *
     * @param string $name
     * @param int|null $postId
     * @return mixed
     */
    public function getMetaValue(string $name, ?int $postId);

    /**
     * @see wpseo_replace_vars()
     *
     * @param string $format
     * @param array|object|\WP_Post|null $post
     * @param array|null $omit
     * @return string
     */
    public function replaceVars(string $format, $post, $omit = []): string;
}
