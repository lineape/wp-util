<?php

namespace EliKellendonk\WpUtil\Proxy;

class ACFProxyImpl implements ACFProxy
{
    /** @inheritDoc */
    public function updateSetting(string $name, $value): bool
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var bool
         */
        return acf_update_setting($name, $value);
    }

    /** @inheritDoc */
    public function getField(
        string $name,
        ?int $postId,
        ?bool $formatValue = true
    ) {
        /** @psalm-suppress UndefinedFunction */
        return get_field($name, $postId, $formatValue);
    }

    /** @inheritDoc */
    public function getFields(int $post_id, $format_value = true)
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var array|false
         */
        return get_fields($post_id, $format_value);
    }

    /** @inheritDoc */
    public function addLocalFieldGroup(array $group): bool
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var bool
         */
        return acf_add_local_field_group($group);
    }
}
