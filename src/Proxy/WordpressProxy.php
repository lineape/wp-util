<?php

namespace EliKellendonk\WpUtil\Proxy;

interface WordpressProxy
{
    /**
     * @see get_option()
     *
     * @param string $option
     * @param mixed $default
     * @return mixed
     */
    public function getOption(string $option, $default);

    /**
     * @see update_option()
     *
     * @param string $option
     * @param mixed $value
     * @param string|bool $autoload
     * @return bool
     */
    public function updateOption(
        string $option,
        $value,
        $autoload = null
    ): bool;

    /**
     * @see add_action()
     *
     * @param string $name
     * @param callable $function
     * @param int|null $priority
     * @param int|null $acceptedArgs
     * @return bool
     */
    public function addAction(
        string $name,
        callable $function,
        ?int $priority = 10,
        ?int $acceptedArgs = 1
    ): bool;

    /**
     * @see add_filter()
     *
     * @param string $name
     * @param callable $function
     * @param int|null $priority
     * @param int|null $acceptedArgs
     * @return bool
     */
    public function addFilter(
        string $name,
        callable $function,
        ?int $priority = 10,
        ?int $acceptedArgs = 1
    ): bool;

    /**
     * @see get_post_meta()
     *
     * @param int|null $postId
     * @param string|null $key
     * @param bool|null $single
     * @return mixed
     */
    public function getPostMeta(
        ?int $postId,
        ?string $key = '',
        ?bool $single = false
    );

    /**
     * @see wp_get_nav_menu_items()
     *
     * @param int|string $menu
     * @param array|null $args
     * @return false|array
     */
    public function getNavMenuItems($menu, ?array $args = []);

    /**
     * @see get_post()
     *
     * @param int|null $post
     * @param string|null $output
     * @param string|null $filter
     * @return array|null
     */
    public function getPost($post, $output = 'OBJECT', $filter = 'raw');

    /**
     * @see get_term()
     *
     * @param int|object $term
     * @param string $taxonomy
     * @param string $output
     * @param string $filter
     * @return mixed
     */
    public function getTerm(
        $term,
        $taxonomy = '',
        $output = 'OBJECT',
        $filter = 'raw'
    );

    /**
     * @see get_site_url()
     *
     * @return string
     */
    public function getSiteUrl(): string;

    /**
     * @see get_permalink()
     *
     * @param int|array|object $post
     * @param bool $leaveName
     * @return string|false
     */
    public function getPermalink($post, bool $leaveName = false);

    /**
     * @see get_post_type_object()
     *
     * @param int|array|string|null $arg
     * @return object|null
     */
    public function getPostType($arg): ?object;

    /**
     * @see add_theme_support()
     *
     * @param string $feature
     * @param array $args
     * @return false|void
     */
    public function addThemeSupport(string $feature, ...$args);

    /**
     * @see get_the_author_meta()
     *
     * @param int $userId
     * @param string $field
     * @return mixed
     */
    public function getAuthorMeta(int $userId, string $field);

    /**
     * @see get_the_author_meta()
     *
     * @param int $userId
     * @param string $postType
     * @param bool $publicOnly
     * @return int
     */
    public function getAuthorPostCount(
        int $userId,
        string $postType = 'post',
        bool $publicOnly = true
    ): int;

    /**
     * @see register_post_type()
     *
     * @param string $postType
     * @param array $args
     */
    public function registerPostType(string $postType, array $args = []): void;

    /**
     * @see register_rest_field()
     *
     * @param string|string[] $object_type
     * @param string $attribute
     * @param array  $args
     */
    public function registerRestField(
        $object_type,
        $attribute,
        $args = []
    ): void;

    /**
     * @see wp_get_attachment_image_src()
     *
     * @param int $id
     * @param string|int[] $size
     * @param bool         $icon
     *
     * @return array|false
     */
    public function getMediaImage(int $id, $size = 'thumbnail', $icon = false);

    /** @see is_admin() */
    public function isAdmin(): bool;

    /**
     * @psalm-suppress UndefinedDocblockClass
     * @see get_current_screen()
     * @return \WP_Screen|null
     */
    public function getCurrentScreen();

    /**
     * @see esc_attr()
     * @param string $text
     * @return string
     */
    public function escAttr(string $text): string;

    /**
     * @psalm-suppress UndefinedDocblockClass
     * @see get_adjacent_post()
     *
     * @param bool $inSameTerm
     * @param string|string[] $excludedTerms
     * @param bool $previous
     * @param string $taxonomy
     *
     * @return null|string|\WP_Post
     */
    public function getAdjacentPost(
        $inSameTerm = false,
        $excludedTerms = '',
        $previous = true,
        $taxonomy = 'category'
    );
}
