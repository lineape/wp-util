<?php

namespace EliKellendonk\WpUtil\Proxy;

class YoastProxyImpl implements YoastProxy
{
    /** @inheritDoc */
    public function getMetaValue(string $name, ?int $postId)
    {
        /** @psalm-suppress UndefinedClass */
        return \WPSEO_Meta::get_value($name, $postId);
    }

    /** @inheritDoc */
    public function replaceVars(string $format, $post, $omit = []): string
    {
        /**
         * @psalm-suppress UndefinedFunction
         * @var string
         */
        return wpseo_replace_vars($format, $post, $omit);
    }
}
