<?php

namespace EliKellendonk\WpUtil\Proxy;

/**
 * ACFProxy abstracts away the Wordpress ACF API so that tests can be written.
 */
interface ACFProxy
{
    /**
     * @see acf_update_setting
     *
     * @param string $name
     * @param mixed $value
     * @return bool
     */
    public function updateSetting(string $name, $value): bool;

    /**
     * @see get_field()
     *
     * @param string $name
     * @param int|null $postId
     * @param bool|null $formatValue
     * @return mixed
     */
    public function getField(
        string $name,
        ?int $postId,
        ?bool $formatValue = true
    );

    /**
     * @see get_fields()
     *
     * @param int $post_id
     * @param boolean|null $format_value
     *
     * @return array|false
     */
    public function getFields(int $post_id, $format_value = true);

    /**
     * @see acf_add_local_field_group()
     *
     * @param array $group
     * @return bool
     */
    public function addLocalFieldGroup(array $group): bool;
}
