<?php

namespace EliKellendonk\WpUtil;

use EliKellendonk\WpUtil\Proxy\WordpressProxy;

class PluginActivator implements Runnable
{
    /** @var string[] */
    private static array $requestedPlugins = [];

    /** @param string[] $plugins */
    public static function activate(array $plugins): void
    {
        static::$requestedPlugins = array_merge(
            static::$requestedPlugins,
            $plugins,
        );
    }

    private WordpressProxy $wp;

    public function __construct(WordpressProxy $wp)
    {
        $this->wp = $wp;
    }

    public function run(): void
    {
        $this->wp->addAction('admin_init', function () {
            /** @var string[] */
            $activePlugins = $this->wp->getOption('active_plugins', []);

            foreach (static::$requestedPlugins as $plugin) {
                if (!in_array($plugin, $activePlugins, true)) {
                    $activePlugins[] = $plugin;
                    $this->wp->updateOption('active_plugins', $activePlugins);
                }
            }
        });
    }
}
