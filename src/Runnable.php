<?php

namespace EliKellendonk\WpUtil;

/**
 * Runnable is an interface for any object that can be told to arbitrarily run.
 * @package EliKellendonk\WpUtil
 */
interface Runnable
{
    public function run(): void;
}
